#!/usr/bin/env python3

# --------------
# TestCollatz.py
# --------------

# pylint: disable = bad-whitespace
# pylint: disable = invalid-name
# pylint: disable = missing-docstring
# pylint: disable = line-too-long
#

# -------
# imports
# -------

from io import StringIO
from unittest import main, TestCase

from Collatz import collatz_read, collatz_print, collatz_solve, cached_collatz_eval

# -----------
# TestCollatz
# -----------


class TestCollatz(TestCase):
    # ----
    # read
    # ----

    def test_read(self):
        s = "1 10\n"
        i, j = collatz_read(s)
        self.assertEqual(i, 1)
        self.assertEqual(j, 10)

    # ----
    # eval
    # ----

    def test_eval_1(self):
        v = cached_collatz_eval(1, 501)
        self.assertEqual(v, 144)

    def test_eval_2(self):
        v = cached_collatz_eval(100, 200)
        self.assertEqual(v, 125)

    def test_eval_3(self):
        v = cached_collatz_eval(201, 210)
        self.assertEqual(v, 89)

    def test_eval_4(self):
        v = cached_collatz_eval(900, 1000)
        self.assertEqual(v, 174)

    def test_eval_5(self):
        v = cached_collatz_eval(945838, 258540)
        self.assertEqual(v, 525)

    def test_eval_6(self):
        v = cached_collatz_eval(796518, 641712)
        self.assertEqual(v, 504)

    # -----
    # print
    # -----

    def test_print(self):
        w = StringIO()
        collatz_print(w, 1, 10, 20)
        self.assertEqual(w.getvalue(), "1 10 20\n")

    # -----
    # solve
    # -----

    def test_solve_1(self):
        r = StringIO("1 10\n100 200\n201 210\n900 1000\n")
        w = StringIO()
        collatz_solve(r, w)
        self.assertEqual(
            w.getvalue(), "1 10 20\n100 200 125\n201 210 89\n900 1000 174\n"
        )

    def test_solve_2(self):
        r = StringIO("229647 163152\n270937 18327\n419751 364930\n568186 486465\n")
        w = StringIO()
        collatz_solve(r, w)
        self.assertEqual(
            w.getvalue(),
            "229647 163152 386\n270937 18327 443\n419751 364930 449\n568186 486465 470\n",
        )

    def test_solve_3(self):
        r = StringIO(
            "401152 122964\n475958 955908\n113873 437205\n959810 399214\n851679 236371\n892644 261403\n"
        )
        w = StringIO()
        collatz_solve(r, w)
        self.assertEqual(
            w.getvalue(),
            "401152 122964 443\n475958 955908 525\n113873 437205 449\n959810 399214 525\n851679 236371 525\n892644 261403 525\n",
        )

    def test_solve_4(self):
        r = StringIO("823437 842555\n244048 312758\n532716 967064\n834348 702000\n")
        w = StringIO()
        collatz_solve(r, w)
        self.assertEqual(
            w.getvalue(),
            "823437 842555 525\n244048 312758 407\n532716 967064 525\n834348 702000 504\n",
        )


# ----
# main
# ----

if __name__ == "__main__":  # pragma: no cover
    main()
