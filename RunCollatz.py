#!/usr/bin/env python3

# -------------
# RunCollatz.py
# -------------

# pylint: disable = bad-whitespace
# pylint: disable = invalid-name
# pylint: disable = missing-docstring

# -------
# imports
# -------

from sys import stdin, stdout
from Collatz import collatz_solve

# from Collatz import test_case_gen

# ----
# main
# ----

if __name__ == "__main__":
    collatz_solve(stdin, stdout)
    # test_case_gen(stdout)
